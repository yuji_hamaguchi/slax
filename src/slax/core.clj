(ns slax.core
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [compojure.handler :as handler]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [clj-http.client :as client]
            [ring.middleware.json :as json]
            [ring.util.response :refer [response]]
            [clojure.data.json :as data-json]
            [clj-time.core :as time-core]
            [clj-time.local :as local-time]
            [clj-time.coerce :as time-coerce]
            [clj-time.format :as time-format]))

;実行例
;curl -XGET -H "Content-Type: application/json" "http://localhost:3001/slax?user_name=foo&token=4BxgxxiihzrSU3KrSBirlTEG&team_domain=uzabase&channel_name=slax&text=tail%20-n2%20%40yuji.hamaguchi%20%7C%20grep%20baz" | native2ascii -reverse -encoding UTF-8 | jq .
;curl -XGET -H "Content-Type: application/json" "http://localhost:3001/slax?user_name=foo&token=4BxgxxiihzrSU3KrSBirlTEG&team_domain=uzabase&channel_name=slax&text=tail%20-n2%20%40yuji.hamaguchi" | native2ascii -reverse -encoding UTF-8 | jq .
(def urls {:tail    "http://localhost:3002/slax/tail"
           :grep    "http://localhost:3003/slax/grep"
           :sort    "http://localhost:3000/api/sort"
           :history "http://localhost:9000/history"
           :ls      "http://localhost:8091/ls"})

(defn execute-command
  [token team_domain channel_name text user_name & sx-univ-objs]
  (println "called with command:" text ".")
  (let [command (first (clojure.string/split text #"\s"))]
    (if-not sx-univ-objs
      (-> (client/get (urls (keyword command))
                      {:query-params {"user_name"    user_name
                                      "token"        (case command
                                                       "ls" "xoxp-2466069044-8547250964-262986284112-aa6312157b99ed352fe4730d443beb5f"
                                                       token)
                                      "team_domain"  team_domain
                                      "channel_name" (case command
                                                       "ls" "C7SCETXCL"
                                                       channel_name)
                                      "text"         text}
                       :debug        true})
          :body
          (data-json/read-str :key-fn keyword))
      (-> (client/post (urls (keyword command))
                       {:query-params {"user_name"    user_name
                                       "token"        token
                                       "team_domain"  team_domain
                                       "channel_name" channel_name
                                       "text"         text}
                        ;:debug-body true
                        ;:debug true
                        :body         (data-json/write-str (case command
                                                             "sort" {:item (first sx-univ-objs)}
                                                             (first sx-univ-objs)))
                        :accept       :json
                        :content-type :json})
          :body
          (data-json/read-str :key-fn keyword)))))

(defn command-handler
  [token team_domain channel_name text user_name]
  (println "called with commands:" text ".")
  (let [commands (map clojure.string/trim (clojure.string/split text #"\|"))
        entry-command (first commands)
        rest-command (rest commands)]
    (let [result (execute-command token team_domain channel_name entry-command user_name)]
      (letfn [(execute-commands
                [result commands]
                (if (zero? (count commands))
                  result
                  (execute-commands (execute-command token team_domain channel_name (first commands) user_name result) (rest commands))))]
        (execute-commands result rest-command)))))

(defn sum-up-universal-objs
  [ms]
  (println "ms:" ms)
  (if (:text ms)
    (do
      (println "ms-text:" (:text ms))
      {:text (apply str (interpose "\n" (:text ms)))})
    (let [text (apply str (map #(str
                                  (time-coerce/from-long (* (Long. (first (clojure.string/split (:ts %) #"\."))) 1000))
                                  " _in_ *#" (:channel-name %) "*"
                                  " _by_ *@" (:username %) "*\n"
                                  " " (:text %) "\n"
                                  "`" (:permalink %) "`\n") ms))]
      {:text text})))

(defn slax-gateway
  [token team_domain channel_name text user_name]
  (println token team_domain channel_name text user_name)
  (when (not (= user_name "slackbot"))
    (if (and (= token "4BxgxxiihzrSU3KrSBirlTEG")
             (= team_domain "uzabase")
             (= channel_name "slax"))
      (response (->> (command-handler token team_domain channel_name text user_name)
                     (sum-up-universal-objs)))
      (response {:text "Authentication Failed"}))))

(defroutes app-routes
           (GET "/slax" [token team_domain channel_name text user_name]
                (slax-gateway token team_domain channel_name text user_name))
           (POST "/slax" [token team_domain channel_name text user_name]
                 (slax-gateway token team_domain channel_name text user_name))
           (route/not-found "Not Found"))

(def app
  (-> (handler/api app-routes)
      (json/wrap-json-body)
      (json/wrap-json-response)))
